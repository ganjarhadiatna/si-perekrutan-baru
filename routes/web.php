<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'divisi'], function () {
    // ui
    Route::get('', 'DivisiController@index')->name('ui-divisi');
    Route::get('/create', 'DivisiController@create')->name('ui-divisi-create');
    Route::get('/edit/{id}', 'DivisiController@edit')->name('ui-divisi-edit');

    // crud
    Route::post('/save', 'DivisiController@save')->name('ui-divisi-save');
    Route::post('/update', 'DivisiController@update')->name('ui-divisi-update');
    Route::post('/delete', 'DivisiController@delete')->name('ui-divisi-delete');
});

Route::group(['prefix' => 'jabatan'], function () {
    // ui
    Route::get('', 'JabatanController@index')->name('ui-jabatan');
    Route::get('/create', 'JabatanController@create')->name('ui-jabatan-create');
    Route::get('/edit/{id}', 'JabatanController@edit')->name('ui-jabatan-edit');

    // crud
    Route::post('/save', 'JabatanController@save')->name('ui-jabatan-save');
    Route::post('/update', 'JabatanController@update')->name('ui-jabatan-update');
    Route::post('/delete', 'JabatanController@delete')->name('ui-jabatan-delete');
});

Route::group(['prefix' => 'karyawan'], function () {
    // ui
    Route::get('', 'KaryawanController@index')->name('ui-karyawan');
    Route::get('/create', 'KaryawanController@create')->name('ui-karyawan-create');
    Route::get('/edit/{id}', 'KaryawanController@edit')->name('ui-karyawan-edit');

    // crud
    Route::post('/save', 'KaryawanController@save')->name('ui-karyawan-save');
    Route::post('/update', 'KaryawanController@update')->name('ui-karyawan-update');
    Route::post('/delete', 'KaryawanController@delete')->name('ui-karyawan-delete');
});

Route::group(['prefix' => 'fptk'], function () {
    // ui
    Route::get('', 'FptkController@index')->name('ui-fptk');
    Route::get('/create', 'FptkController@create')->name('ui-fptk-create');
    Route::get('/edit/{id}', 'FptkController@edit')->name('ui-fptk-edit');

    // crud
    Route::post('/save', 'FptkController@save')->name('ui-fptk-save');
    Route::post('/update', 'FptkController@update')->name('ui-fptk-update');
    Route::post('/delete', 'FptkController@delete')->name('ui-fptk-delete');
});

Route::group(['prefix' => 'lowongan'], function () {
    // ui
    Route::get('', 'LowonganController@index')->name('ui-lowongan');
    Route::get('/create', 'LowonganController@create')->name('ui-lowongan-create');
    Route::get('/edit/{id}', 'LowonganController@edit')->name('ui-lowongan-edit');

    // crud
    Route::post('/save', 'LowonganController@save')->name('ui-lowongan-save');
    Route::post('/update', 'LowonganController@update')->name('ui-lowongan-update');
    Route::post('/delete', 'LowonganController@delete')->name('ui-lowongan-delete');
});

Route::group(['prefix' => 'pelamar'], function () {
    // ui
    Route::get('', 'PelamarController@index')->name('ui-pelamar');
    Route::get('/create', 'PelamarController@create')->name('ui-pelamar-create');
    Route::get('/edit/{id}', 'PelamarController@edit')->name('ui-pelamar-edit');

    // crud
    Route::post('/save', 'PelamarController@save')->name('ui-pelamar-save');
    Route::post('/update', 'PelamarController@update')->name('ui-pelamar-update');
    Route::post('/delete', 'PelamarController@delete')->name('ui-pelamar-delete');
});

Route::group(['prefix' => 'data-nilai'], function () {
    // ui
    Route::get('', 'DataNilaiController@index')->name('ui-data-nilai');
    Route::get('/create', 'DataNilaiController@create')->name('ui-data-nilai-create');
    Route::get('/edit/{id}', 'DataNilaiController@edit')->name('ui-data-nilai-edit');

    // crud
    Route::post('/save', 'DataNilaiController@save')->name('ui-data-nilai-save');
    Route::post('/update', 'DataNilaiController@update')->name('ui-data-nilai-update');
    Route::post('/delete', 'DataNilaiController@delete')->name('ui-data-nilai-delete');
});

Route::group(['prefix' => 'kriteria-kinerja'], function () {
    // ui
    Route::get('', 'KriteriaKinerjaController@index')->name('ui-kriteria-kinerja');
    Route::get('/create', 'KriteriaKinerjaController@create')->name('ui-kriteria-kinerja-create');
    Route::get('/edit/{id}', 'KriteriaKinerjaController@edit')->name('ui-kriteria-kinerja-edit');

    // crud
    Route::post('/save', 'KriteriaKinerjaController@save')->name('ui-kriteria-kinerja-save');
    Route::post('/update', 'KriteriaKinerjaController@update')->name('ui-kriteria-kinerja-update');
    Route::post('/delete', 'KriteriaKinerjaController@delete')->name('ui-kriteria-kinerja-delete');
});

Route::group(['prefix' => 'kriteria-rekruitmen'], function () {
    // ui
    Route::get('', 'KriteriaRekruitmenController@index')->name('ui-kriteria-rekruitmen');
    Route::get('/create', 'KriteriaRekruitmenController@create')->name('ui-kriteria-rekruitmen-create');
    Route::get('/edit/{id}', 'KriteriaRekruitmenController@edit')->name('ui-kriteria-rekruitmen-edit'); 

    // crud
    Route::post('/save', 'KriteriaRekruitmenController@save')->name('ui-kriteria-rekruitmen-save');
    Route::post('/update', 'KriteriaRekruitmenController@update')->name('ui-kriteria-rekruitmen-update');
    Route::post('/delete', 'KriteriaRekruitmenController@delete')->name('ui-kriteria-rekruitmen-delete');
});

Route::group(['prefix' => 'penilaian-rekruitmen'], function () {
    // ui
    Route::get('', 'PenilaianRekruitmenController@index')->name('ui-penilaian-rekruitmen');
    Route::get('/history/{id}', 'PenilaianRekruitmenController@history')->name('ui-penilaian-rekruitmen-history'); 
    Route::get('/create', 'PenilaianRekruitmenController@create')->name('ui-penilaian-rekruitmen-create');
    Route::get('/edit/{id}', 'PenilaianRekruitmenController@edit')->name('ui-penilaian-rekruitmen-edit'); 

    // crud
    Route::post('/save', 'PenilaianRekruitmenController@save')->name('ui-penilaian-rekruitmen-save');
    Route::post('/update', 'PenilaianRekruitmenController@update')->name('ui-penilaian-rekruitmen-update');
    Route::post('/delete', 'PenilaianRekruitmenController@delete')->name('ui-penilaian-rekruitmen-delete');
});

Route::group(['prefix' => 'hasil-rekruitmen'], function () {
    // ui
    Route::get('', 'HasilRekruitmenController@index')->name('ui-hasil-rekruitmen');
    Route::get('/generate', 'HasilRekruitmenController@generate')->name('ui-hasil-rekruitmen-generate');
    Route::get('/create', 'HasilRekruitmenController@create')->name('ui-hasil-rekruitmen-create');
    Route::get('/history/{id}', 'HasilRekruitmenController@history')->name('ui-hasil-rekruitmen-history'); 
    Route::get('/edit/{id}', 'HasilRekruitmenController@edit')->name('ui-hasil-rekruitmen-edit'); 

    // crud
    Route::post('/save', 'HasilRekruitmenController@save')->name('ui-hasil-rekruitmen-save');
    Route::post('/update', 'HasilRekruitmenController@update')->name('ui-hasil-rekruitmen-update');
    Route::post('/delete', 'HasilRekruitmenController@delete')->name('ui-hasil-rekruitmen-delete');
});
