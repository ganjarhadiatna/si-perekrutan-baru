<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Karyawan extends Authenticatable
{
    use Notifiable;
    
    protected $table = "karyawan";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'NIK', 'nama', 'email', 'password', 'no_telp', 'alamat', 'tanggal_masuk', 'status', 'id_jabatan', 'id_divisi'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'karyawan.id',
            'karyawan.NIK',
            'karyawan.nama',
            'karyawan.email',
            'karyawan.status',
            'karyawan.alamat',
            'karyawan.tanggal_masuk',
            'karyawan.no_telp',
            'karyawan.id_jabatan',
            'karyawan.id_divisi',
            'jabatan.nama as jabatan',
            'divisi.nama as divisi',
            'jabatan.keterangan as keterangan_jabatan',
            'divisi.keterangan as keterangan_divisi'
        )
        ->join('jabatan', 'jabatan.id', '=', 'karyawan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'karyawan.id_divisi')
        ->orderBy('karyawan.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetByID($query, $id)
    {
        return $this
        ->select(
            'karyawan.id',
            'karyawan.NIK',
            'karyawan.nama',
            'karyawan.email',
            'karyawan.status',
            'karyawan.alamat',
            'karyawan.tanggal_masuk',
            'karyawan.no_telp',
            'karyawan.id_jabatan',
            'karyawan.id_divisi',
            'jabatan.nama as jabatan',
            'divisi.nama as divisi',
            'jabatan.keterangan as keterangan_jabatan',
            'divisi.keterangan as keterangan_ divisi'
        )
        ->join('jabatan', 'jabatan.id', '=', 'karyawan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'karyawan.id_divisi')
        ->where('karyawan.id', $id)
        ->first();
    }

    public function scopeGetDivisiJabatan($query, $id)
    {
        return $this
        ->select(
            'karyawan.id',
            'karyawan.NIK',
            'karyawan.nama',
            'karyawan.email',
            'karyawan.status',
            'karyawan.alamat',
            'karyawan.tanggal_masuk',
            'karyawan.no_telp',
            'karyawan.id_jabatan',
            'karyawan.id_divisi',
            'jabatan.nama as jabatan',
            'divisi.nama as divisi',
            'jabatan.keterangan as keterangan_jabatan',
            'divisi.keterangan as keterangan_ divisi'
        )
        ->join('jabatan', 'jabatan.id', '=', 'karyawan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'karyawan.id_divisi')
        ->where('karyawan.id', $id)
        ->first();
    }

    public function scopeCheckUser($query, $email)
    {
        return $this
        ->select(
            'karyawan.id',
            'karyawan.nama',
            'karyawan.email',
            'karyawan.status',
            'karyawan.password',
            'karyawan.id_jabatan',
            'karyawan.id_divisi',
            'jabatan.nama as jabatan',
            'divisi.nama as divisi'
        )
        ->join('jabatan', 'jabatan.id', '=', 'karyawan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'karyawan.id_divisi')
        ->where('email', $email)
        ->first();
    }
}
