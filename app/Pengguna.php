<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengguna extends Authenticatable
{
    use Notifiable;

    protected $table = 'pengguna';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'id_karyawan'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function scopeGetDivisiJabatan($query, $id)
    {
        return $this
        ->select(
            'karyawan.id',
            'karyawan.NIK',
            'karyawan.nama',
            'karyawan.email',
            'karyawan.status',
            'karyawan.alamat',
            'karyawan.tanggal_masuk',
            'karyawan.no_telp',
            'karyawan.id_jabatan',
            'karyawan.id_divisi',
            'jabatan.nama as jabatan',
            'divisi.nama as divisi',
            'jabatan.keterangan as keterangan_jabatan',
            'divisi.keterangan as keterangan_ divisi'
        )
        ->join('karyawan', 'karyawan.id', '=', 'pengguna.id_karyawan')
        ->join('jabatan', 'jabatan.id', '=', 'karyawan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'karyawan.id_divisi')
        ->where('karyawan.id', $id)
        ->first();
    }
}
