<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilRekruitmen extends Model
{
    protected $table = 'hasil_rekruitmen';

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'hasil_rekruitmen.id',
            'hasil_rekruitmen.vektor_s',
            'hasil_rekruitmen.vektor_v',
            'hasil_rekruitmen.created_at',
            'hasil_rekruitmen.updated_at',
            'pelamar.id as id_pelamar',
            'pelamar.nama as nama_pelamar',
            'karyawan.nama as nama_karyawan',
            'jabatan.keterangan as jabatan',
            'divisi.keterangan as divisi'
        )
        ->orderBy('hasil_rekruitmen.vektor_v', 'desc')
        ->join('karyawan', 'karyawan.id', '=', 'hasil_rekruitmen.id_karyawan')
        ->join('pelamar', 'pelamar.id', '=', 'hasil_rekruitmen.id_pelamar')
        ->join('lowongan', 'lowongan.id', '=', 'pelamar.id_lowongan')
        ->join('jabatan', 'jabatan.id', '=', 'lowongan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'lowongan.id_divisi')
        ->paginate($limit);
    }
}
