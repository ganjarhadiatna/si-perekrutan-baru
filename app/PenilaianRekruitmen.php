<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenilaianRekruitmen extends Model
{
    protected $table = 'penilaian_rekruitmen';

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'penilaian_rekruitmen.id',
            'penilaian_rekruitmen.nilai',
            'penilaian_rekruitmen.created_at',
            'penilaian_rekruitmen.updated_at',
            'pelamar.nama as nama_pelamar',
            'karyawan.nama as nama_karyawan',
            'kriteria_rekruitmen.nama as nama_kriteria_rekruitmen',
            'kriteria_rekruitmen.bobot as bobot_kriteria_rekruitmen'
        )
        ->orderBy('penilaian_rekruitmen.id', 'desc')
        ->join('pelamar', 'pelamar.id', '=', 'penilaian_rekruitmen.id_pelamar')
        ->join('karyawan', 'karyawan.id', '=', 'penilaian_rekruitmen.id_karyawan')
        ->join('kriteria_rekruitmen', 'kriteria_rekruitmen.id', '=', 'penilaian_rekruitmen.id_kriteria_rekruitmen')
        ->paginate($limit);
    }

    public function scopeGetById($query, $id, $limit)
    {
        return $this
        ->select(
            'penilaian_rekruitmen.id',
            'penilaian_rekruitmen.nilai',
            'penilaian_rekruitmen.created_at',
            'penilaian_rekruitmen.updated_at',
            'pelamar.nama as nama_pelamar',
            'karyawan.nama as nama_karyawan',
            'kriteria_rekruitmen.nama as nama_kriteria_rekruitmen',
            'kriteria_rekruitmen.bobot as bobot_kriteria_rekruitmen'
        )
        ->orderBy('penilaian_rekruitmen.id', 'asc')
        ->join('pelamar', 'pelamar.id', '=', 'penilaian_rekruitmen.id_pelamar')
        ->join('karyawan', 'karyawan.id', '=', 'penilaian_rekruitmen.id_karyawan')
        ->join('kriteria_rekruitmen', 'kriteria_rekruitmen.id', '=', 'penilaian_rekruitmen.id_kriteria_rekruitmen')
        ->where('penilaian_rekruitmen.id_pelamar', $id)
        ->paginate($limit);
    }
}
