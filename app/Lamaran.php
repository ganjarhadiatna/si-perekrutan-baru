<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lamaran extends Model
{
    protected $table = 'lamaran';

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'pelamar.id',
            'pelamar.no_ktp',
            'pelamar.nama',
            'pelamar.email',
            'pelamar.file_cv',
            'pelamar.no_telp'
        )
        ->orderBy('lamaran.id', 'desc')
        ->join('pelamar', 'pelamar.id', '=', 'lamaran.id_pelamar')
        ->paginate($limit);
    }
}
