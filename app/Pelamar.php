<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelamar extends Model
{
    protected $table = 'pelamar';

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'pelamar.id',
            'pelamar.no_ktp',
            'pelamar.nama',
            'pelamar.email',
            'pelamar.file_cv',
            'pelamar.no_telp',
            'pelamar.status',
            'pelamar.created_at',
            'pelamar.updated_at',
            'lowongan.judul',
            'jabatan.keterangan as jabatan',
            'divisi.keterangan as divisi'
        )
        ->join('lowongan', 'lowongan.id', '=', 'pelamar.id_lowongan')
        ->join('jabatan', 'jabatan.id', '=', 'lowongan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'lowongan.id_divisi')
        ->orderBy('pelamar.id', 'desc')
        ->paginate($limit);
    }
}
