<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    protected $table = 'lowongan';

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'lowongan.id',
            'lowongan.judul',
            'lowongan.deskripsi',
            'lowongan.status',
            'lowongan.kuota',
            'lowongan.tanggal_dibuka',
            'lowongan.tanggal_ditutup',
            'lowongan.created_at',
            'lowongan.updated_at',
            'jabatan.keterangan as jabatan',
            'divisi.keterangan as divisi',
            'karyawan.nama as karyawan'
        )
        ->join('jabatan', 'jabatan.id', '=', 'lowongan.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'lowongan.id_divisi')
        ->join('karyawan', 'karyawan.id', '=', 'lowongan.id_karyawan')
        ->orderBy('lowongan.id', 'desc')
        ->paginate($limit);
    }
}
