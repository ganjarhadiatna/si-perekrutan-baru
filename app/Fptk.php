<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fptk extends Model
{
    protected $table = 'fptk';

    public function scopeGetAll($query, $limit)
    {
        return $this
        ->select(
            'fptk.id',
            'fptk.judul',
            'fptk.kriteria',
            'fptk.status',
            'fptk.kuota',
            'fptk.created_at',
            'fptk.updated_at',
            'jabatan.keterangan as jabatan',
            'divisi.keterangan as divisi'
        )
        ->join('jabatan', 'jabatan.id', '=', 'fptk.id_jabatan')
        ->join('divisi', 'divisi.id', '=', 'fptk.id_divisi')
        ->orderBy('fptk.id', 'desc')
        ->paginate($limit);
    }
}
