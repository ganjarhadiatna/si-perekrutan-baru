<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lowongan;
use App\Jabatan;
use App\Divisi;
use Auth;

class LowonganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Lowongan::GetAll(5);
        return view('lowongan.index', ['data' => $data]);
    }

    public function create()
    {
        $jabatan = Jabatan::get();
        $divisi = Divisi::get();
        return view('lowongan.form', ['jabatan' => $jabatan, 'divisi' => $divisi]);
    }

    public function edit($id)
    {
        $data = Lowongan::where(['id' => $id])->first();
        $jabatan = Jabatan::get();
        $divisi = Divisi::get();
        return view('lowongan.form', ['data' => $data, 'jabatan' => $jabatan, 'divisi' => $divisi]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|min:0|max:150',
            'deskripsi' => 'required|min:0',
            'kuota' => 'required|min:0|max:4',
            'tanggal_dibuka' => 'required',
            'tanggal_ditutup' => 'required',
            'status' => 'required',
            'id_jabatan' => 'required',
            'id_divisi' => 'required'
        ]);

        $data = [
            'judul' => $request->input('judul'),
            'deskripsi' => $request->input('deskripsi'),
            'kuota' => $request->input('kuota'),
            'status' => $request->input('status'),
            'tanggal_dibuka' => $request->input('tanggal_dibuka'),
            'tanggal_ditutup' => $request->input('tanggal_ditutup'),
            'id_jabatan' => $request->input('id_jabatan'),
            'id_divisi' => $request->input('id_divisi'),
            'id_karyawan' => Auth::user()->GetDivisiJabatan(Auth::user()->id)->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Lowongan::insert($data);

        if ($service) 
        {
            return redirect('/lowongan');
        }
        else 
        {
            return redirect('/lowongan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'judul' => 'required|min:0|max:150',
            'deskripsi' => 'required|min:0',
            'kuota' => 'required|min:0|max:4',
            'tanggal_dibuka' => 'required',
            'tanggal_ditutup' => 'required',
            'status' => 'required',
            'id_jabatan' => 'required',
            'id_divisi' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'judul' => $request->input('judul'),
            'deskripsi' => $request->input('deskripsi'),
            'kuota' => $request->input('kuota'),
            'status' => $request->input('status'),
            'tanggal_dibuka' => $request->input('tanggal_dibuka'),
            'tanggal_ditutup' => $request->input('tanggal_ditutup'),
            'id_jabatan' => $request->input('id_jabatan'),
            'id_divisi' => $request->input('id_divisi'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Lowongan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/lowongan');
        }
        else 
        {
            return redirect('/lowongan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Lowongan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/lowongan');
        }
        else 
        {
            return redirect('/lowongan');
        }
    }
}
