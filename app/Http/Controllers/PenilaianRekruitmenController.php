<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PenilaianRekruitmen;
use App\KriteriaRekruitmen;
use App\Lowongan;
use App\HasilRekruitmen;
use Auth;

class PenilaianRekruitmenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // public function index()
    // {
    //     $data = Lowongan::GetAll(5);
    //     return view('penilaianrekruitmen.lowongan', ['data' => $data]);
    // }

    public function index()
    {
        $data = PenilaianRekruitmen::GetAll(5);
        return view('penilaianrekruitmen.index', ['data' => $data]);
    }

    public function create()
    {
        $kriteria = KriteriaRekruitmen::get();
        return view('penilaianrekruitmen.form', ['kriteria' => $kriteria]);
    }

    public function history($id)
    {
        $data = PenilaianRekruitmen::GetByID($id, 5);
        return view('penilaianrekruitmen.index', ['data' => $data]);
    }

    public function edit($id)
    {
        $data = PenilaianRekruitmen::where(['id' => $id])->first();
        return view('penilaianrekruitmen.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'id_pelamar' => 'required|min:0|max:5',
        ]);

        $id_pelamar = $request->input('id_pelamar');
        $id_karyawan = Auth::user()->GetDivisiJabatan(Auth::user()->id)->id;

        $bobot = [];
        $nilai = [];
        $total_bobot = 0;
        
        $kriteria = KriteriaRekruitmen::get();

        foreach($kriteria as $dt) {
            $total_bobot += $request->input('bobot-' . $dt->id);
            array_push($nilai, $request->input('nilai-' . $dt->id));
            array_push($bobot, $request->input('bobot-' . $dt->id));
        }

        $n = count($bobot);
        $nilai_w = [];

        for ($i=0; $i < $n; $i++) { 
            $w = $bobot[$i] / $total_bobot;
            array_push($nilai_w, round($w, 2));
        }

        $nilai_s = [];

        for ($i=0; $i < $n; $i++) { 
            $s = pow($nilai[$i], $nilai_w[$i]);
            array_push($nilai_s, round($s, 2));
        }

        $nilai_vectos_s = 0;
        for ($i=0; $i < $n; $i++) {
            if ($i == 0) {
                $nilai_vectos_s = $nilai_s[$i];
            } else {
                $nilai_vectos_s *= $nilai_s[$i];
            }
        }

        $data = [];
        $i = 0;
        foreach($kriteria as $dt) {
            $payload = [
                'nilai' => $nilai[$i],
                'id_kriteria_rekruitmen' => $dt->id,
                'id_pelamar' => $id_pelamar,
                'id_karyawan' => $id_karyawan,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];
            array_push($data, $payload);
            $i++;
        }

        $data2 = [
            'vektor_s' => $nilai_vectos_s,
            'id_pelamar' => $id_pelamar,
            'id_karyawan' => $id_karyawan,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        PenilaianRekruitmen::where(['id_pelamar' => $id_pelamar])->delete();
        HasilRekruitmen::where(['id_pelamar' => $id_pelamar])->delete();

        $service = PenilaianRekruitmen::insert($data);
        $service2 = HasilRekruitmen::insert($data2);

        if ($service && $service2) 
        {
            return redirect('/penilaian-rekruitmen');
        }
        else 
        {
            return redirect('/penilaian-rekruitmen/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'bobot' => 'required|min:0|max:5',
            'nama' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'bobot' => $request->input('bobot'),
            'nama' => $request->input('nama'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = PenilaianRekruitmen::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/penilaian-rekruitmen');
        }
        else 
        {
            return redirect('/penilaian-rekruitmen/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = PenilaianRekruitmen::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/penilaian-rekruitmen');
        }
        else 
        {
            return redirect('/penilaian-rekruitmen');
        }
    }
}
