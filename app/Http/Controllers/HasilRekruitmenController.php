<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PenilaianRekruitmen;
use App\KriteriaRekruitmen;
use App\HasilRekruitmen;
use App\Pelamar;
use Auth;

class HasilRekruitmenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = HasilRekruitmen::GetAll(5);
        return view('hasilrekruitmen.index', ['data' => $data]);
    }

    public function history($id)
    {
        $data = PenilaianRekruitmen::GetByID($id, 5);
        return view('penilaianrekruitmen.index', ['data' => $data]);
    }

    public function generate()
    {
        $data = HasilRekruitmen::get();
        $total_vektor = 0;

        foreach ($data as $dt) {
            $total_vektor += $dt->vektor_s;
        }

        foreach ($data as $dt) {
            $s = $dt->vektor_s / $total_vektor;
            $pp = [
                "vektor_v" => $s,
                "updated_at" => date('Y-m-d H:i:s')
            ];
            HasilRekruitmen::where(['id' => $dt->id])->update($pp);
        }

        return redirect('/hasil-rekruitmen');
    }

    public function create()
    {
        $kriteria = KriteriaRekruitmen::get();
        $pelamar = Pelamar::where(['status' => 'disetujui'])->get();
        return view('hasilrekruitmen.form', ['kriteria' => $kriteria, 'pelamar' => $pelamar]);
    }

    public function edit($id)
    {
        $data = HasilRekruitmen::where(['id' => $id])->first();
        $kriteria = KriteriaRekruitmen::get();
        $pelamar = Pelamar::where(['status' => 'disetujui'])->get();
        return view('hasilrekruitmen.form', ['data' => $data, 'kriteria' => $kriteria, 'pelamar' => $pelamar]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'id_pelamar' => 'required|min:0|max:5',
        ]);

        $id_pelamar = $request->input('id_pelamar');
        $id_karyawan = Auth::user()->GetDivisiJabatan(Auth::user()->id)->id;

        $bobot = [];
        $nilai = [];
        $total_bobot = 0;
        
        $kriteria = KriteriaRekruitmen::get();

        foreach($kriteria as $dt) {
            $total_bobot += $request->input('bobot-' . $dt->id);
            array_push($nilai, $request->input('nilai-' . $dt->id));
            array_push($bobot, $request->input('bobot-' . $dt->id));
        }

        $n = count($bobot);
        $nilai_w = [];

        for ($i=0; $i < $n; $i++) { 
            $w = $bobot[$i] / $total_bobot;
            array_push($nilai_w, round($w, 2));
        }

        $nilai_s = [];

        for ($i=0; $i < $n; $i++) { 
            $s = pow($nilai[$i], $nilai_w[$i]);
            array_push($nilai_s, round($s, 2));
        }

        $nilai_vectos_s = 0;
        for ($i=0; $i < $n; $i++) {
            if ($i == 0) {
                $nilai_vectos_s = $nilai_s[$i];
            } else {
                $nilai_vectos_s *= $nilai_s[$i];
            }
        }

        $data = [];
        $i = 0;
        foreach($kriteria as $dt) {
            $payload = [
                'nilai' => $nilai[$i],
                'id_kriteria_rekruitmen' => $dt->id,
                'id_pelamar' => $id_pelamar,
                'id_karyawan' => $id_karyawan,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];
            array_push($data, $payload);
            $i++;
        }

        $data2 = [
            'vektor_s' => $nilai_vectos_s,
            'id_pelamar' => $id_pelamar,
            'id_karyawan' => $id_karyawan,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        PenilaianRekruitmen::where(['id_pelamar' => $id_pelamar])->delete();
        HasilRekruitmen::where(['id_pelamar' => $id_pelamar])->delete();

        $service = PenilaianRekruitmen::insert($data);
        $service2 = HasilRekruitmen::insert($data2);

        if ($service && $service2) 
        {
            return redirect('/hasil-rekruitmen');
        }
        else 
        {
            return redirect('/hasil-rekruitmen/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id_pelamar' => 'required|min:0|max:5',
        ]);

        $id_pelamar = $request->input('id_pelamar');
        $id_karyawan = Auth::user()->GetDivisiJabatan(Auth::user()->id)->id;

        $bobot = [];
        $nilai = [];
        $total_bobot = 0;
        
        $kriteria = KriteriaRekruitmen::get();

        foreach($kriteria as $dt) {
            $total_bobot += $request->input('bobot-' . $dt->id);
            array_push($nilai, $request->input('nilai-' . $dt->id));
            array_push($bobot, $request->input('bobot-' . $dt->id));
        }

        $n = count($bobot);
        $nilai_w = [];

        for ($i=0; $i < $n; $i++) { 
            $w = $bobot[$i] / $total_bobot;
            array_push($nilai_w, round($w, 2));
        }

        $nilai_s = [];

        for ($i=0; $i < $n; $i++) { 
            $s = pow($nilai[$i], $nilai_w[$i]);
            array_push($nilai_s, round($s, 2));
        }

        $nilai_vectos_s = 0;
        for ($i=0; $i < $n; $i++) {
            if ($i == 0) {
                $nilai_vectos_s = $nilai_s[$i];
            } else {
                $nilai_vectos_s *= $nilai_s[$i];
            }
        }

        $data = [];
        $i = 0;
        foreach($kriteria as $dt) {
            $payload = [
                'nilai' => $nilai[$i],
                'id_kriteria_rekruitmen' => $dt->id,
                'id_pelamar' => $id_pelamar,
                'id_karyawan' => $id_karyawan,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ];
            array_push($data, $payload);
            $i++;
        }

        $data2 = [
            'vektor_s' => $nilai_vectos_s,
            'id_pelamar' => $id_pelamar,
            'id_karyawan' => $id_karyawan,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        PenilaianRekruitmen::where(['id_pelamar' => $id_pelamar])->delete();
        HasilRekruitmen::where(['id_pelamar' => $id_pelamar])->delete();

        $service = PenilaianRekruitmen::insert($data);
        $service2 = HasilRekruitmen::insert($data2);

        if ($service && $service2) 
        {
            return redirect('/hasil-rekruitmen');
        }
        else 
        {
            return redirect('/hasil-rekruitmen/create');
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = HasilRekruitmen::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/hasil-rekruitmen');
        }
        else 
        {
            return redirect('/hasil-rekruitmen');
        }
    }
}
