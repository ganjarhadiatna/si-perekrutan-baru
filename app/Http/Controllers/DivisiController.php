<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Divisi;

class DivisiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Divisi::paginate(5);
        return view('divisi.index', ['data' => $data]);
    }

    public function create()
    {
        return view('divisi.form');
    }

    public function edit($id)
    {
        $data = Divisi::where(['id' => $id])->first();
        return view('divisi.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'required|min:0|max:150'
        ]);

        $data = [
            'nama' => $request->input('nama'),
            'keterangan' => $request->input('keterangan'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Divisi::insert($data);

        if ($service) 
        {
            return redirect('/divisi');
        }
        else 
        {
            return redirect('/divisi/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'nama' => $request->input('nama'),
            'keterangan' => $request->input('keterangan'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Divisi::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/divisi');
        }
        else 
        {
            return redirect('/divisi/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Divisi::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/divisi');
        }
        else 
        {
            return redirect('/divisi');
        }
    }
}
