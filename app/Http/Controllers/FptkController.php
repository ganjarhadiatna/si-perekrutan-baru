<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fptk;
use App\Jabatan;
use App\Divisi;

class FptkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Fptk::GetAll(5);
        return view('fptk.index', ['data' => $data]);
    }

    public function create()
    {
        $jabatan = Jabatan::get();
        $divisi = Divisi::get();
        return view('fptk.form', ['jabatan' => $jabatan, 'divisi' => $divisi]);
    }

    public function edit($id)
    {
        $data = Fptk::where(['id' => $id])->first();
        $jabatan = Jabatan::get();
        $divisi = Divisi::get();
        return view('fptk.form', ['data' => $data, 'jabatan' => $jabatan, 'divisi' => $divisi]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|min:0|max:150',
            'kriteria' => 'required|min:0',
            'kuota' => 'required|min:0|max:4',
            'status' => 'required',
            'id_jabatan' => 'required',
            'id_divisi' => 'required'
        ]);

        $data = [
            'judul' => $request->input('judul'),
            'kriteria' => $request->input('kriteria'),
            'kuota' => $request->input('kuota'),
            'status' => $request->input('status'),
            'id_jabatan' => $request->input('id_jabatan'),
            'id_divisi' => $request->input('id_divisi'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Fptk::insert($data);

        if ($service) 
        {
            return redirect('/fptk');
        }
        else 
        {
            return redirect('/fptk/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'judul' => 'required|min:0|max:150',
            'kriteria' => 'required|min:0',
            'kuota' => 'required|min:0|max:4',
            'status' => 'required',
            'id_jabatan' => 'required',
            'id_divisi' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'judul' => $request->input('judul'),
            'kriteria' => $request->input('kriteria'),
            'kuota' => $request->input('kuota'),
            'status' => $request->input('status'),
            'id_jabatan' => $request->input('id_jabatan'),
            'id_divisi' => $request->input('id_divisi'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Fptk::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/fptk');
        }
        else 
        {
            return redirect('/fptk/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Fptk::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/fptk');
        }
        else 
        {
            return redirect('/fptk');
        }
    }
}
