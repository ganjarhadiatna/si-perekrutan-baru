<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;

class JabatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Jabatan::paginate(5);
        return view('jabatan.index', ['data' => $data]);
    }

    public function create()
    {
        return view('jabatan.form');
    }

    public function edit($id)
    {
        $data = Jabatan::where(['id' => $id])->first();
        return view('jabatan.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'required|min:0|max:150'
        ]);

        $data = [
            'nama' => $request->input('nama'),
            'keterangan' => $request->input('keterangan'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Jabatan::insert($data);

        if ($service) 
        {
            return redirect('/jabatan');
        }
        else 
        {
            return redirect('/jabatan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required|min:0|max:150',
            'keterangan' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'nama' => $request->input('nama'),
            'keterangan' => $request->input('keterangan'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Jabatan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/jabatan');
        }
        else 
        {
            return redirect('/jabatan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Jabatan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/jabatan');
        }
        else 
        {
            return redirect('/jabatan');
        }
    }
}
