<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KriteriaRekruitmen;

class KriteriaRekruitmenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = KriteriaRekruitmen::orderBy('id', 'desc')->paginate(5);
        return view('kriteriarekruitmen.index', ['data' => $data]);
    }

    public function create()
    {
        return view('kriteriarekruitmen.form');
    }

    public function edit($id)
    {
        $data = KriteriaRekruitmen::where(['id' => $id])->first();
        return view('kriteriarekruitmen.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'bobot' => 'required|min:0|max:5',
            'nama' => 'required|min:0|max:150'
        ]);

        $data = [
            'bobot' => $request->input('bobot'),
            'nama' => $request->input('nama'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = KriteriaRekruitmen::insert($data);

        if ($service) 
        {
            return redirect('/kriteria-rekruitmen');
        }
        else 
        {
            return redirect('/kriteria-rekruitmen/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'bobot' => 'required|min:0|max:5',
            'nama' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'bobot' => $request->input('bobot'),
            'nama' => $request->input('nama'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = KriteriaRekruitmen::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/kriteria-rekruitmen');
        }
        else 
        {
            return redirect('/kriteria-rekruitmen/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = KriteriaRekruitmen::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/kriteria-rekruitmen');
        }
        else 
        {
            return redirect('/kriteria-rekruitmen');
        }
    }
}
