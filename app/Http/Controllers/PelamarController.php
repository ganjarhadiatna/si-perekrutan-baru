<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelamar;
use App\Divisi;
use App\Jabatan;
use App\Lowongan;

class PelamarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Pelamar::GetAll(5);
        return view('pelamar.index', ['data' => $data]);
    }

    public function create()
    {
        $lowongan = Lowongan::get();
        return view('pelamar.form', ['lowongan' => $lowongan]);
    }

    public function edit($id)
    {
        $data = Pelamar::where('id', $id)->first();
        $lowongan = Lowongan::get();
        return view('pelamar.form', ['data' => $data, 'lowongan' => $lowongan]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'no_ktp' => 'required|min:0|max:16|unique:pelamar',
            'nama' => 'required|min:0|max:150',
            'email' => 'required|string|email|max:255',
            'no_telp' => 'required|min:0|max:15',
            'file_cv' => 'required|min:0|max:255',
            'catatan' => 'min:0|max:255',
            'status' => 'required',
            'id_lowongan' => 'required'
        ]);

        $data = [
            'no_ktp' => $request->input('no_ktp'),
            'nama' => $request->input('nama'),
            'email' => $request->input('email'),
            'no_telp' => $request->input('no_telp'),
            'file_cv' => $request->input('file_cv'),
            'catatan' => $request->input('catatan'),
            'status' => $request->input('status'),
            'id_lowongan' => $request->input('id_lowongan'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pelamar::insert($data);

        if ($service) 
        {
            return redirect('/pelamar');
        }
        else 
        {
            return redirect('/pelamar/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required|min:0|max:150',
            'email' => 'required|string|email|max:255',
            'no_telp' => 'required|min:0|max:15',
            'file_cv' => 'required|min:0|max:255',
            'catatan' => 'min:0|max:255',
            'status' => 'required',
            'id_lowongan' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'nama' => $request->input('nama'),
            'email' => $request->input('email'),
            'no_telp' => $request->input('no_telp'),
            'file_cv' => $request->input('file_cv'),
            'catatan' => $request->input('catatan'),
            'status' => $request->input('status'),
            'id_lowongan' => $request->input('id_lowongan'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pelamar::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/pelamar');
        }
        else 
        {
            return redirect('/pelamar/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Pelamar::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/pelamar');
        }
        else 
        {
            return redirect('/pelamar');
        }
    }
}
