<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KriteriaKinerja;

class KriteriaKinerjaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = KriteriaKinerja::orderBy('id', 'desc')->paginate(5);
        return view('kriteriakinerja.index', ['data' => $data]);
    }

    public function create()
    {
        return view('kriteriakinerja.form');
    }

    public function edit($id)
    {
        $data = KriteriaKinerja::where(['id' => $id])->first();
        return view('kriteriakinerja.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required|min:0|max:5',
            'keterangan' => 'required|min:0|max:150',
            'nama' => 'required|min:0|max:150'
        ]);

        $data = [
            'kode' => $request->input('kode'),
            'keterangan' => $request->input('keterangan'),
            'nama' => $request->input('nama'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = KriteriaKinerja::insert($data);

        if ($service) 
        {
            return redirect('/kriteria-kinerja');
        }
        else 
        {
            return redirect('/kriteria-kinerja/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'kode' => 'required|min:0|max:5',
            'keterangan' => 'required|min:0|max:150',
            'nama' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'kode' => $request->input('kode'),
            'keterangan' => $request->input('keterangan'),
            'nama' => $request->input('nama'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = KriteriaKinerja::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/kriteria-kinerja');
        }
        else 
        {
            return redirect('/kriteria-kinerja/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = KriteriaKinerja::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/kriteria-kinerja');
        }
        else 
        {
            return redirect('/kriteria-kinerja');
        }
    }
}
