<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataNilai;

class DataNilaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = DataNilai::orderBy('id', 'desc')->paginate(5);
        return view('datanilai.index', ['data' => $data]);
    }

    public function create()
    {
        return view('datanilai.form');
    }

    public function edit($id)
    {
        $data = DataNilai::where(['id' => $id])->first();
        return view('datanilai.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'nilai' => 'required',
            'keterangan' => 'required|min:0|max:150',
            'definisi' => 'required|min:0|max:150'
        ]);

        $data = [
            'nilai' => $request->input('nilai'),
            'keterangan' => $request->input('keterangan'),
            'definisi' => $request->input('definisi'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = DataNilai::insert($data);

        if ($service) 
        {
            return redirect('/data-nilai');
        }
        else 
        {
            return redirect('/data-nilai/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nilai' => 'required',
            'keterangan' => 'required|min:0|max:150',
            'definisi' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'nilai' => $request->input('nilai'),
            'keterangan' => $request->input('keterangan'),
            'definisi' => $request->input('definisi'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = DataNilai::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/data-nilai');
        }
        else 
        {
            return redirect('/data-nilai/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = DataNilai::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/data-nilai');
        }
        else 
        {
            return redirect('/data-nilai');
        }
    }
}
