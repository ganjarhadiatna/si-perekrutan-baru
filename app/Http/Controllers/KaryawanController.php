<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Karyawan;
use App\Divisi;
use App\Jabatan;

class KaryawanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Karyawan::GetAll(5);
        return view('karyawan.index', ['data' => $data]);
    }

    public function create()
    {
        $divisi = Divisi::get();
        $jabatan = Jabatan::get();
        return view('karyawan.form', ['divisi' => $divisi, 'jabatan' => $jabatan]);
    }

    public function edit($id)
    {
        $data = Karyawan::GetByID($id);
        $divisi = Divisi::get();
        $jabatan = Jabatan::get();
        return view('karyawan.form', ['data' => $data, 'divisi' => $divisi, 'jabatan' => $jabatan]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'NIK' => 'required|min:0|max:16',
            'nama' => 'required|min:0|max:150',
            'email' => 'required|string|email|max:255|unique:karyawan',
            'password' => 'required|string|min:8',
            'no_telp' => 'required|min:0|max:15',
            'alamat' => 'required|min:0|max:255',
            'tanggal_masuk' => 'required|date',
            'status' => 'required',
            'id_jabatan' => 'required',
            'id_divisi' => 'required'
        ]);

        $data = [
            'NIK' => $request->input('NIK'),
            'nama' => $request->input('nama'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'no_telp' => $request->input('no_telp'),
            'alamat' => $request->input('alamat'),
            'tanggal_masuk' => $request->input('tanggal_masuk'),
            'status' => $request->input('status'),
            'id_divisi' => $request->input('id_divisi'),
            'id_jabatan' => $request->input('id_jabatan'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Karyawan::insert($data);

        if ($service) 
        {
            return redirect('/karyawan');
        }
        else 
        {
            return redirect('/karyawan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'NIK' => 'required|min:0|max:16',
            'nama' => 'required|min:0|max:150',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8',
            'no_telp' => 'required|min:0|max:15',
            'alamat' => 'required|min:0|max:255',
            'tanggal_masuk' => 'required|date',
            'status' => 'required',
            'id_jabatan' => 'required',
            'id_divisi' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'NIK' => $request->input('NIK'),
            'nama' => $request->input('nama'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'no_telp' => $request->input('no_telp'),
            'alamat' => $request->input('alamat'),
            'tanggal_masuk' => $request->input('tanggal_masuk'),
            'status' => $request->input('status'),
            'id_divisi' => $request->input('id_divisi'),
            'id_jabatan' => $request->input('id_jabatan'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Karyawan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/karyawan');
        }
        else 
        {
            return redirect('/karyawan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Karyawan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/karyawan');
        }
        else 
        {
            return redirect('/karyawan');
        }
    }
}
