<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLowonganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lowongan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('judul', 150);
            $table->text('deskripsi');
            $table->string('kuota', 32);
            $table->date('tanggal_dibuka');
            $table->date('tanggal_ditutup');
            $table->enum('status', ['dibuka', 'ditutup'])->default('dibuka');
            $table->unsignedBigInteger('id_karyawan');
            $table->unsignedBigInteger('id_jabatan');
            $table->unsignedBigInteger('id_divisi');
            $table->timestamps();

            // modifier
            $table->foreign('id_karyawan')->references('id')->on('karyawan');
            $table->foreign('id_jabatan')->references('id')->on('jabatan');
            $table->foreign('id_divisi')->references('id')->on('divisi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lowongan');
    }
}
