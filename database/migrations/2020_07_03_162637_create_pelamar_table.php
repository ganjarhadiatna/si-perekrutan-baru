<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelamarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelamar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_ktp', 16);
            $table->string('nama', 150);
            $table->string('email', 150);
            $table->string('no_telp', 15);
            $table->string('file_cv', 255);
            $table->string('catatan', 255);
            $table->unsignedBigInteger('id_lowongan');
            $table->enum('status', ['menunggu', 'disetujui', 'ditolak'])->default('menunggu');
            $table->timestamps();

            // modifier
            $table->unique(['no_ktp', 'email']);
            $table->foreign('id_lowongan')->references('id')->on('lowongan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelamar');
    }
}
