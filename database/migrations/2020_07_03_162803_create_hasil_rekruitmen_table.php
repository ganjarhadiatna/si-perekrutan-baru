<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHasilRekruitmenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_rekruitmen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('vektor_s', 8, 2)->nullable();
            $table->double('vektor_v', 8, 2)->nullable();
            $table->unsignedBigInteger('id_karyawan');
            $table->unsignedBigInteger('id_pelamar');
            $table->timestamps();

            // modifier
            $table->unique(['id_karyawan', 'id_pelamar']);
            $table->foreign('id_karyawan')->references('id')->on('karyawan');
            $table->foreign('id_pelamar')->references('id')->on('pelamar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_rekruitmen');
    }
}
