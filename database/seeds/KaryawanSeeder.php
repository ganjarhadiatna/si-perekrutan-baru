<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'NIK' => 'SPL9905033662',
                'nama' => 'Gunawan',
                'email' => 'gunawan@gmail.com',
                'no_telp' => '082221221221',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '5',
                'id_divisi' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'NIK' => 'SPL9905033234',
                'nama' => 'Bambang',
                'email' => 'bambang@gmail.com',
                'no_telp' => '08522122444',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '4',
                'id_divisi' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'NIK' => 'SPL9905033999',
                'nama' => 'Surya',
                'email' => 'surya@gmail.com',
                'no_telp' => '08522122555',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '1',
                'id_divisi' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'NIK' => 'SPL9905033334',
                'nama' => 'Bima',
                'email' => 'bima@gmail.com',
                'no_telp' => '08522122555',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '2',
                'id_divisi' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'NIK' => 'SPL99050333022',
                'nama' => 'Asep',
                'email' => 'asep@gmail.com',
                'no_telp' => '08522122444',
                'alamat' => 'Jl. Mana aja lah',
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 'tetap',
                'id_jabatan' => '3',
                'id_divisi' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('karyawan')->insert($data);
    }
}
