<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PelamarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'nama' => 'Jajang Rohimat',
                'no_ktp' => '0392885885885',
                'email' => 'binder123@gmail.com',
                'file_cv' => 'cv.pdf',
                'no_telp' => '085321543765',
                'catatan' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                'id_lowongan' => '2',
                'status' => 'disetujui',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'nama' => 'Asep Sumpena',
                'no_ktp' => '0392885885333',
                'email' => 'binder122@gmail.com',
                'file_cv' => 'cv.pdf',
                'no_telp' => '085321543765',
                'catatan' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                'id_lowongan' => '2',
                'status' => 'disetujui',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'nama' => 'Ari Sutina',
                'no_ktp' => '0392885888999',
                'email' => 'binder123@gmail.com',
                'file_cv' => 'cv.pdf',
                'no_telp' => '085321543765',
                'catatan' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                'id_lowongan' => '2',
                'status' => 'menunggu',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'nama' => 'Aceng Suraceng',
                'no_ktp' => '0392885884433',
                'email' => 'binder124@gmail.com',
                'file_cv' => 'cv.pdf',
                'no_telp' => '085321543765',
                'catatan' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
                'id_lowongan' => '3',
                'status' => 'menunggu',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('pelamar')->insert($data);
    }
}
