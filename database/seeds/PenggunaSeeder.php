<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'email' => 'gunawan@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'id_karyawan' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'email' => 'bambang@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'id_karyawan' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'email' => 'surya@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'id_karyawan' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'email' => 'bima@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'id_karyawan' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'email' => 'asep@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'id_karyawan' => '5',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('pengguna')->insert($data);
    }
}
