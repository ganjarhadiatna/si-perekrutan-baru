<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenilaianRekruitmenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'nilai' => '3.00',
                'id_kriteria_rekruitmen' => '1',
                'id_karyawan' => '2',
                'id_pelamar' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'nilai' => '3.00',
                'id_kriteria_rekruitmen' => '2',
                'id_karyawan' => '2',
                'id_pelamar' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'nilai' => '3.00',
                'id_kriteria_rekruitmen' => '3',
                'id_karyawan' => '2',
                'id_pelamar' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'nilai' => '2.00',
                'id_kriteria_rekruitmen' => '4',
                'id_karyawan' => '2',
                'id_pelamar' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'nilai' => '2.00',
                'id_kriteria_rekruitmen' => '5',
                'id_karyawan' => '2',
                'id_pelamar' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '6',
                'nilai' => '3.00',
                'id_kriteria_rekruitmen' => '1',
                'id_karyawan' => '2',
                'id_pelamar' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '7',
                'nilai' => '3.00',
                'id_kriteria_rekruitmen' => '2',
                'id_karyawan' => '2',
                'id_pelamar' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '8',
                'nilai' => '3.00',
                'id_kriteria_rekruitmen' => '3',
                'id_karyawan' => '2',
                'id_pelamar' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '9',
                'nilai' => '2.00',
                'id_kriteria_rekruitmen' => '4',
                'id_karyawan' => '2',
                'id_pelamar' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '10',
                'nilai' => '2.00',
                'id_kriteria_rekruitmen' => '5',
                'id_karyawan' => '2',
                'id_pelamar' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
        ];

        DB::table('penilaian_rekruitmen')->insert($data);
    }
}
