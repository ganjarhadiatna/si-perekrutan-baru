<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataNilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'nilai' => 2.5,
                'keterangan' => 'bobot nilai',
                'definisi' => 'bobot nilai',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'nilai' => 3.5,
                'keterangan' => 'bobot nilai',
                'definisi' => 'bobot nilai',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'nilai' => 5.0,
                'keterangan' => 'bobot nilai',
                'definisi' => 'bobot nilai',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'nilai' => 6,
                'keterangan' => 'bobot nilai',
                'definisi' => 'bobot nilai',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('data_nilai')->insert($data);
    }
}
