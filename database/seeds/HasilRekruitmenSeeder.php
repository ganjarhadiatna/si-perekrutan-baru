<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HasilRekruitmenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'vektor_s' => '72.32',
                'vektor_v' => '0.32',
                'id_karyawan' => '2',
                'id_pelamar' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'vektor_s' => '75.61',
                'vektor_v' => '0.35',
                'id_karyawan' => '2',
                'id_pelamar' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
        ];

        DB::table('hasil_rekruitmen')->insert($data);
    }
}
