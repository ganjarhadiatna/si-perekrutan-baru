<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KriteriaKinerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'kode' => 'KK001',
                'nama' => 'Kriteria kinerja',
                'keterangan' => 'Kriteria kinerja',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'kode' => 'KK002',
                'nama' => 'Kriteria kinerja',
                'keterangan' => 'Kriteria kinerja',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'kode' => 'KK003',
                'nama' => 'Kriteria kinerja',
                'keterangan' => 'Kriteria kinerja',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'kode' => 'KK004',
                'nama' => 'Kriteria kinerja',
                'keterangan' => 'Kriteria kinerja',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('kriteria_kinerja')->insert($data);
    }
}
