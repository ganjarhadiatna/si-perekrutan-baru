@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Karyawan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Karyawan</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-karyawan-update') }} @else {{ route('ui-karyawan-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputNIK">NIK</label>
                            <input type="text" value="@if(isset($data)) {{ $data->NIK }} @endif" class="form-control @error('NIK') is-invalid @enderror" id="inputNIK" name="NIK" required>
                            @error('NIK')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputNama">Nama lengkap</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nama }} @endif" class="form-control @error('nama') is-invalid @enderror" id="inputNama" name="nama" required>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputEmail">Alamat Email</label>
                            <input type="email" value="@if(isset($data)) {{ $data->email }} @endif" class="form-control @error('email') is-invalid @enderror" id="inputEmail" name="email" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputPassword">Password baru</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="inputPassword" name="password" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputNoTelp">No telpon</label>
                            <input type="text" value="@if(isset($data)) {{ $data->no_telp }} @endif" class="form-control @error('no_telp') is-invalid @enderror" id="inputNoTelp" name="no_telp" required>
                            @error('no_telp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputAlamat">Alamat rumah</label>
                            <input type="text" value="@if(isset($data)) {{ $data->alamat }} @endif" class="form-control @error('alamat') is-invalid @enderror" id="inputAlamat" name="alamat" required>
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTanggalMasuk">Tanggal masuk</label>
                            <input type="date" value="@if(isset($data)) {{ $data->tanggal_masuk }} @endif" class="form-control @error('tanggal_masuk') is-invalid @enderror" id="inputTanggalMasuk" name="tanggal_masuk" required>
                            @error('tanggal_masuk')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                @if(isset($data))
                                    @if($data->status == 'tetap')
                                        <option value="tetap" selected>Karyawan Tetap</option>
                                        <option value="kontrak">Karyawan Kontrak</option>
                                    @else
                                        <option value="tetap">Karyawan Tetap</option>
                                        <option value="kontrak" selected>Karyawan Kontrak</option>
                                    @endif
                                @else
                                    <option value="tetap">Karyawan Tetap</option>
                                    <option value="kontrak">Karyawan Kontrak</option>
                                @endif
                                </select>
                            </div>
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputDivisi">Divisi</label>
                                <select value="@if(isset($data)) {{ $data->id_divisi }} @endif" class="form-control @error('id_divisi') is-invalid @enderror" id="inputDivisi" name="id_divisi">
                                @foreach($divisi as $dt)
                                    <option value="{{ $dt->id }}">{{ $dt->keterangan }}</option>
                                @endforeach
                                </select>
                            </div>
                            @error('id_divisi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputJabatan">Jabatan</label>
                                <select value="@if(isset($data)) {{ $data->id_jabatan }} @endif" class="form-control @error('id_jabatan') is-invalid @enderror" id="inputJabatan" name="id_jabatan">
                                @foreach($jabatan as $dt)
                                    <option value="{{ $dt->id }}">{{ $dt->keterangan }}</option>
                                @endforeach
                                </select>
                            </div>
                            @error('id_jabatan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
