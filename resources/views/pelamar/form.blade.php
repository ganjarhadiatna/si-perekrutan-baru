@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Pelamar</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Pelamar</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-pelamar-update') }} @else {{ route('ui-pelamar-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputKtp">No. KTP</label>
                            @if(isset($data))
                                <input type="text" value="@if(isset($data)) {{ $data->no_ktp }} @endif" class="form-control @error('no_ktp') is-invalid @enderror" id="inputKtp" name="no_ktp" required readonly>
                            @else
                                <input type="text" value="@if(isset($data)) {{ $data->no_ktp }} @endif" class="form-control @error('no_ktp') is-invalid @enderror" id="inputKtp" name="no_ktp" required>
                            @endif
                            @error('no_ktp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputNama">Nama</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nama }} @endif" class="form-control @error('nama') is-invalid @enderror" id="inputNama" name="nama" required>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input type="email" value="@if(isset($data)) {{ $data->email }} @endif" class="form-control @error('email') is-invalid @enderror" id="inputEmail" name="email" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputNoTelp">No. Telp</label>
                            <input type="text" value="@if(isset($data)) {{ $data->no_telp }} @endif" class="form-control @error('no_telp') is-invalid @enderror" id="inputNoTelp" name="no_telp" required>
                            @error('no_telp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputFileCv">Dokumen CV</label>
                            <input type="text" value="@if(isset($data)) {{ $data->file_cv }} @endif" class="form-control @error('file_cv') is-invalid @enderror" id="inputFileCv" name="file_cv" required>
                            @error('file_cv')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputCatatan">Catatan</label>
                            <textarea type="text" class="form-control @error('catatan') is-invalid @enderror" id="inputCatatan" name="catatan" required>@if(isset($data)) {{ $data->catatan }} @endif</textarea>
                            @error('catatan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                    @if(isset($data))
                                        @if($data->status == 'menunggu')
                                            <option value="menunggu" selected>Menunggu</option>
                                            <option value="disetujui">Disetujui</option>
                                            <option value="ditolak">Ditolak</option>
                                        @endif
                                        @if($data->status == 'disetujui')
                                            <option value="menunggu">Menunggu</option>
                                            <option value="disetujui" selected>Disetujui</option>
                                            <option value="ditolak">Ditolak</option>
                                        @endif
                                        @if($data->status == 'ditolak')
                                            <option value="menunggu">Menunggu</option>
                                            <option value="disetujui">Disetujui</option>
                                            <option value="ditolak" selected>Ditolak</option>
                                        @endif
                                    @else
                                        <option value="menunggu">Menunggu</option>
                                        <option value="disetujui">Disetujui</option>
                                        <option value="ditolak">Ditolak</option>
                                    @endif
                                </select>
                            </div>
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputLowongan">Lowongan</label>
                                <select value="@if(isset($data)) {{ $data->id_lowongan }} @endif" class="form-control @error('id_lowongan') is-invalid @enderror" id="inputLowongan" name="id_lowongan">
                                @foreach($lowongan as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->id_lowongan)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->judul }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->judul }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->judul }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('id_lowongan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
