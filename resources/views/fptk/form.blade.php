@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah FPTK</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah FPTK</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-fptk-update') }} @else {{ route('ui-fptk-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputJudul">Judul</label>
                            <input type="text" value="@if(isset($data)) {{ $data->judul }} @endif" class="form-control @error('judul') is-invalid @enderror" id="inputJudul" name="judul" required>
                            @error('judul')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputKriter">Kriteria</label>
                            <textarea type="text" class="form-control @error('kriteria') is-invalid @enderror" id="inputKriter" name="kriteria" required>@if(isset($data)) {{ $data->kriteria }} @endif</textarea>
                            @error('kriteria')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputKuota">Kuota</label>
                            <input type="text" value="@if(isset($data)) {{ $data->kuota }} @endif" class="form-control @error('kuota') is-invalid @enderror" id="inputKuota" name="kuota" required>
                            @error('kuota')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        @if(Auth::user()->GetDivisiJabatan(Auth::user()->id)->jabatan == 'hrd')
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="inputStatus">Status</label>
                                    <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                        @if(isset($data))
                                            @if($data->status == 'menunggu')
                                                <option value="menunggu" selected>Menunggu</option>
                                                <option value="disetujui">Disetujui</option>
                                                <option value="ditolak">Ditolak</option>
                                            @endif
                                            @if($data->status == 'disetujui')
                                                <option value="menunggu">Menunggu</option>
                                                <option value="disetujui" selected>Disetujui</option>
                                                <option value="ditolak">Ditolak</option>
                                            @endif
                                            @if($data->status == 'ditolak')
                                                <option value="menunggu">Menunggu</option>
                                                <option value="disetujui">Disetujui</option>
                                                <option value="ditolak" selected>Ditolak</option>
                                            @endif
                                        @else
                                            <option value="menunggu">Menunggu</option>
                                            <option value="disetujui">Disetujui</option>
                                            <option value="ditolak">Ditolak</option>
                                        @endif
                                    </select>
                                </div>
                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        @else
                            <input type="hidden" name="status" value="menunggu">
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputDivisi">Divisi</label>
                                <select value="@if(isset($data)) {{ $data->id_divisi }} @endif" class="form-control @error('id_divisi') is-invalid @enderror" id="inputDivisi" name="id_divisi">
                                @foreach($divisi as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->id_divisi)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->keterangan }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->keterangan }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->keterangan }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('id_divisi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputJabatan">Jabatan</label>
                                <select value="@if(isset($data)) {{ $data->id_jabatan }} @endif" class="form-control @error('id_jabatan') is-invalid @enderror" id="inputJabatan" name="id_jabatan">
                                @foreach($jabatan as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->id_jabatan)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->keterangan }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->keterangan }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->keterangan }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('id_jabatan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
