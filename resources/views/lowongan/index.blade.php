@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Lowongan</h3>
                    </div>
                    <div class="ml-auto">
                        <a href="{{ route('ui-lowongan-create') }}" class="btn btn-primary">
                            <i class="fa fa-lw fa-plus"></i>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Judul</th>
                                <th scope="col">Kuota</th>
                                <th scope="col">Status</th>
                                <th scope="col">Divisi</th>
                                <th scope="col">Jabatan</th>
                                <th scope="col">Tanggal Dibuka</th>
                                <th scope="col">Tanggal Ditutup</th>
                                <th scope="col">Dibuat Oleh</th>
                                <th width="120"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->judul }}</td>
                                    <td>{{ $dt->kuota }}</td>
                                    <td>{{ $dt->status }}</td>
                                    <td>{{ $dt->divisi }}</td>
                                    <td>{{ $dt->jabatan }}</td>
                                    <td>{{ $dt->tanggal_dibuka }}</td>
                                    <td>{{ $dt->tanggal_ditutup }}</td>
                                    <td>{{ $dt->karyawan }}</td>
                                    <td>
                                        <a href="{{ route('ui-lowongan-edit', $dt->id) }}" class="btn btn-warning">
                                            <i class="fa fa-lw fa-pencil-alt"></i>
                                        </a>
                                        
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                <div class="modal-body">
                                                    Data akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        <a class="btn btn-danger" 
                                                            href="{{ route('ui-lowongan-delete') }}"
                                                            onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                            >
                                                            Lanjutkan
                                                        </a>

                                                        <form id="id-form-{{ $dt->id }}" action="{{ route('ui-lowongan-delete') }}" method="POST" style="display: block;">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
