@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Kriteria Rekruitmen</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Kriteria Rekruitmen</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-kriteria-rekruitmen-update') }} @else {{ route('ui-kriteria-rekruitmen-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputNama">Nama</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nama }} @endif" class="form-control @error('nama') is-invalid @enderror" id="inputNama" name="nama" required>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputBobot">Bobot</label>
                            <input type="text" value="@if(isset($data)) {{ $data->bobot }} @endif" class="form-control @error('bobot') is-invalid @enderror" id="inputBobot" name="bobot" required>
                            @error('bobot')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
