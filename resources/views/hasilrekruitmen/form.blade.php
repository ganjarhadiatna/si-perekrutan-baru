@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Penilaian Rekruitmen</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Penilaian Rekruitmen</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-hasil-rekruitmen-update') }} @else {{ route('ui-hasil-rekruitmen-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputPelamar">Pilih satu pelamar</label>
                                <select value="@if(isset($data)) {{ $data->id_pelamar }} @endif" class="form-control @error('id_pelamar') is-invalid @enderror" id="inputPelamar" name="id_pelamar">
                                    @foreach($pelamar as $dt)
                                        @if(isset($data))
                                            @if($dt->id == $data->id_pelamar)
                                                <option value="{{ $dt->id }}" selected>{{ $dt->nama }}</option>
                                            @else
                                                <option value="{{ $dt->id }}">{{ $dt->nama }}</option>
                                            @endif
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @error('id_pelamar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        @foreach($kriteria as $dt)
                            <div class="form-group" style="padding-top: 10px; padding-bottom: 10px;">
                                <h5 for="inputKriteria">{{ $dt->nama }}</h5>
                                <div class="wrapper">
                                    <div class="container">
                                        <label for="inputKriteria">Bobot</label>
                                        <input type="text" value="{{ $dt->bobot }}" class="form-control @error('bobot') is-invalid @enderror" id="inputKriteria" name="bobot-{{ $dt->id }}" required readonly>
                                    </div>
                                    <div class="container">
                                        <label for="inputNilai">Penilaian</label>
                                        <!-- <input type="text" value="{{ $dt->nilai }}" class="form-control @error('nilai') is-invalid @enderror" id="inputNilai" name="nilai" required> -->
                                        <input type="text" value="{{ $dt->nilai }}" class="form-control @error('nilai') is-invalid @enderror" id="inputNilai" name="nilai-{{ $dt->id }}" required>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
