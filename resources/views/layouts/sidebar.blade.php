<nav id="sidebar">
    <div class="sidebar-header">
        <h3>{{ Auth::user()->GetDivisiJabatan(Auth::user()->id)->keterangan_jabatan }}</h3>
    </div>

    <!-- <ul class="list-unstyled components">
        <li>
            <a href="{{ route('home') }}">Dashboard</a>
        </li>
    </ul> -->

    @if(Auth::user()->GetDivisiJabatan(Auth::user()->id)->jabatan == 'admin')
        <ul class="list-unstyled components">
            <!-- <p>MENU</p> -->
            <li>
                <a href="{{ route('home') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('ui-divisi') }}">Divisi</a>
            </li>
            <li>
                <a href="{{ route('ui-jabatan') }}">Jabatan</a>
            </li>
            <li>
                <a href="{{ route('ui-karyawan') }}">Karyawan</a>
            </li>
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Laporan</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Hasil Rekruitmen</a>
                    </li>
                    <li>
                        <a href="#">Hasil Kinerja</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif

    @if(Auth::user()->GetDivisiJabatan(Auth::user()->id)->jabatan == 'hrd')
        <ul class="list-unstyled components">
            <!-- <p>MENU</p> -->
            <li>
                <a href="{{ route('home') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('ui-fptk') }}">Pengajuan FPTK</a>
            </li>
            <li>
                <a href="{{ route('ui-lowongan') }}">Lowongan Pekerjaan</a>
            </li>
            <li>
                <a href="{{ route('ui-pelamar') }}">Data Pelamar</a>
            </li>
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Rekruitmen</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="{{ route('ui-kriteria-rekruitmen') }}">Kriteria Pembobotan</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-hasil-rekruitmen') }}">Penilaian Rekruitmen</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif

    @if(Auth::user()->GetDivisiJabatan(Auth::user()->id)->jabatan == 'kepala-divisi')
        <ul class="list-unstyled components">
            <li>
                <a href="{{ route('home') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('ui-fptk') }}">Pengajuan FPTK</a>
            </li>
            <li>
                <a href="#menuMasterdata" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Masterdata</a>
                <ul class="collapse list-unstyled" id="menuMasterdata">
                    <li>
                        <a href="{{ route('ui-data-nilai') }}">Data Nilai</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-kriteria-kinerja') }}">Kriteria Kinerja</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#menuPenilaian" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Penilaian</a>
                <ul class="collapse list-unstyled" id="menuPenilaian">
                    <li>
                        <a href="#">Penilaian Kinerja Karyawan</a>
                    </li>
                    <li>
                        <a href="#">Hasil Seleksi Karyawan</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif
</nav>