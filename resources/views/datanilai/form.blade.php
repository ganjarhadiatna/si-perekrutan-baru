@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Data Nilai</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Data Nilai</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-data-nilai-update') }} @else {{ route('ui-data-nilai-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputNilai">Nilai</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nilai }} @endif" class="form-control @error('nilai') is-invalid @enderror" id="inputNilai" name="nilai" required>
                            @error('nilai')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputKeterangan">Keterangan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->keterangan }} @endif" class="form-control @error('keterangan') is-invalid @enderror" id="inputKeterangan" name="keterangan" required>
                            @error('keterangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputDefinisi">Definisi</label>
                            <input type="text" value="@if(isset($data)) {{ $data->definisi }} @endif" class="form-control @error('definisi') is-invalid @enderror" id="inputDefinisi" name="definisi" required>
                            @error('definisi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
