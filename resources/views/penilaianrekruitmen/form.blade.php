@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Penilaian Rekruitmen</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Penilaian Rekruitmen</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-penilaian-rekruitmen-update') }} @else {{ route('ui-penilaian-rekruitmen-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        @foreach($kriteria as $dt)
                            <div class="form-group" style="padding-top: 10px; padding-bottom: 10px;">
                                <h5 for="inputKriteria">{{ $dt->nama }}</h5>
                                <div class="wrapper">
                                    <div class="container">
                                        <label for="inputKriteria">Bobot</label>
                                        <input type="text" value="{{ $dt->bobot }}" class="form-control @error('bobot') is-invalid @enderror" id="inputKriteria" name="bobot" required readonly>
                                    </div>
                                    <div class="container">
                                        <label for="inputNilai">Penilaian</label>
                                        <input type="text" value="{{ $dt->nilai }}" class="form-control @error('nilai') is-invalid @enderror" id="inputNilai" name="nilai" required>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
